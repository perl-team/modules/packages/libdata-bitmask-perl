Source: libdata-bitmask-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Dominique Dumont <dod@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdata-bitmask-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdata-bitmask-perl.git
Homepage: https://metacpan.org/release/Data-BitMask
Rules-Requires-Root: no

Package: libdata-bitmask-perl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${perl:Depends}
Description: bitmask manipulation class
 Data::BitMask is a Perl module to create bitmask manipulator objects
 that can be used to create bitmask values based on a list of
 constants, as well as to break apart masks using those constants. The
 advantages are that you don't have to pollute namespaces to use
 constants, you can ensure that only appropriate constants are used for
 specific masks, you can easily break apart and explain masks, and in
 general it is much easier for the user to interact with masks.
